# 환경설정
## JDK 설치
## 새로운 workspace 생성
## JDK설정
Windows > Preferences > Java > Installed JREs에서 jre가 아닌 jdk로 설정
## 파일 인코딩 설정
Windows > Preferences > General > Workspace에서 Text file encoding을 “UTF-8”로 변경
## 프로젝트 import
1. 왼쪽 Package Explorer에서 오른쪽 마우스 > Import
    > Maven > Existing Maven Project
    > Browse.. 클릭 > 압축해제한 폴더 선택 > Finish
